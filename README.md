### Mirum Drupal 8 test

Please read below for our test task. This is formatted as a user story, which is how we brief technical tasks here at Mirum. Please do ask if anything is unclear, or you need some help!

**User story**   
As a user, I would like to know the current time in Helsinki, New York and
London, so that I can plan my schedule.

**Expected outcome**   
- A custom Drupal 8 module that outputs the current time in the three locations.  
- The output should be visually appealing, however this will be left open to interpretation.  
- The completed module should be pushed back to this git repo.

**Tips**.   
- Use the Drupal examples module to find out how to generate custom blocks from your module.   
- The date can be generated either from PHP or JS - it's your choice.

**Helpful reading**   
https://git-scm.com/docs/user-manual.html  
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date  
https://www.drupal.org/project/examples.
